@extends('layouts.app')

@section('content')
    <div class="jumbotron text-left" id="vueApp" style="padding:5px;">
        <div class="panel panel-default">
            <div class="panel-body">
                <router-view></router-view>
            </div>
         </div>
    </div>
@endsection
