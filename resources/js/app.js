
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.axios.defaults.baseURL = '/';

//modules externes

//permet de faire un spa (single page app) en changeant le component qui est affiché
import VueRouter from 'vue-router';
Vue.use(VueRouter);

//permet d'afficher des alerts
import VueSweetalert2 from 'vue-sweetalert2';
Vue.use(VueSweetalert2);

//permet d'afficher un selecteur (pull down)
import vSelect from 'vue-select'
Vue.component('v-select', vSelect)

//permet d'afficher une pagination sur les pages d'index.
import Paginate from 'vuejs-paginate'
Vue.component('paginate', Paginate)

// Définition des routes pour VueRouter

const routes = [ //todo: créer des fichiers .js avec les groupes de routes et les importer.
    { path: '/', redirect: '/home'},
    { path: '/home', component: require('./components/HomeComponent.vue') },

    { path: '/impressions',
        name:'impressionsIndex',
        component: require('./components/Impressions/ImpressionsIndex.vue'),
        children: [
            {
                name: 'impressions/liste',
                path: 'liste/:idUser',
                component: require('./components/Impressions/ImpressionsListe.vue'),
                props: true
            },
            {
                name: 'impressions/afficher',
                path: 'afficher/:idImpression',
                component: require('./components/Impressions/ImpressionsAfficher.vue'),
                props: true
            },
            {
                name: 'impressions/modifier',
                path: 'modifier/:idImpression',
                component: require('./components/Impressions/ImpressionsModifier.vue'),
                props: true
            },
            {
                name: 'impressions/creer',
                path: 'creer',
                component: require('./components/Impressions/ImpressionsCreer.vue'),
            }


        ]
    },
    { path: '/filaments',
        name:'filamentsIndex',
        component: require('./components/Filaments/FilamentsIndex.vue'),
        children: [
            {
                name: 'filaments/liste',
                path: 'liste',
                component: require('./components/Filaments/FilamentsListe.vue'),
            },
            {
                name: 'filaments/afficher',
                path: 'afficher/:id',
                component: require('./components/Filaments/FilamentsAfficher.vue'),
                props: true
            },
            {
                name: 'filaments/modifier',
                path: 'modifier/:id',
                component: require('./components/Filaments/FilamentsModifier.vue'),
                props: true
            },
            {
                name: 'filaments/creer',
                path: 'creer',
                component: require('./components/Filaments/FilamentsCreer.vue'),
            }

        ]
    },

    { path: '/usagers',
        name:'usagersIndex',
        component: require('./components/Usagers/UsagersIndex.vue'),
        children: [
            {
                name: 'usagers/liste',
                path: 'liste',
                component: require('./components/Usagers/UsagersListe.vue'),
            },
        /**
         *  {
                name: 'usagers/afficher',
                path: 'afficher/:id',
                component: require('./components/Usager/UsagersAfficher.vue'),
                props: true
            },
*/
        ]
    }

];

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Vue.component('example-component', require('./components/ExampleComponent.vue'));

const router = new VueRouter({
    routes // equivalent a 'routes: routes'
});

const app = new Vue({
    router,
    el: '#vueApp'
});
