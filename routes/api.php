<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//TODO: ajouter https://stackoverflow.com/questions/32246359/ajax-middleware/32268128

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['auth']], function() {
    //Impressions

    Route::get('impressions/listePourUsager/{id}', ['as' => 'impressions.index', 'uses' => 'ImpressionController@listePourUsagerApi']);
    Route::get('impressions/getApi/{id}', ['as' => 'impressions.get', 'uses' => 'ImpressionController@getApi']);
    Route::post('impressions/storeApi/{id}', ['as' => 'impressions.store', 'uses' => 'ImpressionController@storeApi']);
    Route::post('impressions/createApi', ['as' => 'impressions.create', 'uses' => 'ImpressionController@createApi']);
    Route::get('impressions/deleteApi/{id}', ['as' => 'impressions.delete', 'uses' => 'ImpressionController@deleteApi']);
    Route::get('impressions/dupliquerApi/{id}', ['as' => 'impressions.dupliquer', 'uses' => 'ImpressionController@dupliquerApi']);
    Route::get('impressions/reservations/{date}/{periode}', ['as' => 'impressions.reservations', 'uses' => 'ImpressionController@reservations']);

   //Filaments
    Route::get('filaments/listeApi', ['as' => 'filaments.liste', 'uses' => 'FilamentController@listeApi']);
    Route::get('filaments/getApi/{id}', ['as' => 'filaments.get', 'uses' => 'FilamentController@getApi']);
    Route::post('filaments/storeApi/{id}', ['as' => 'filaments.store', 'uses' => 'FilamentController@storeApi']);
    Route::post('filaments/createApi', ['as' => 'filaments.create', 'uses' => 'FilamentController@createApi']);
    Route::get('filaments/deleteApi/{id}', ['as' => 'filaments.delete', 'uses' => 'FilamentController@deleteApi']);
    Route::get('filaments/listePourSelecteurApi', ['as' => 'filaments.liste', 'uses' => 'FilamentController@listePourSelecteurApi']);
    Route::post('filaments/creerEtAssocierApi', ['as' => 'filaments.creerEtAssocierApi', 'uses' => 'FilamentController@creerEtAssocierApi']);

    //Users
    Route::get('usagers/listeApi', ['as' => 'usagers.liste', 'uses' => 'UserController@listeApi']);
    Route::get('usagers/getApi/{id}', ['as' => 'usagers.get', 'uses' => 'UserController@getApi']);
});
