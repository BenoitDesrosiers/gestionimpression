<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImpressionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('impressions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('proprietaire_id');
            $table->foreign('proprietaire_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
            $table->string('nom',80);
            $table->string('description',255);
            $table->string('url',255)->nullable();
            $table->string('couleur',30)->nullable();
            $table->string('resultat',500)->nullable();
            $table->string('config',500)->nullable();
            $table->string('tempsDebut');
            $table->string('tempsFin');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('impressions');
    }
}
