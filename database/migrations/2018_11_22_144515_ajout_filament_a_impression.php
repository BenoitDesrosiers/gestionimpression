<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AjoutFilamentAImpression extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Ajout de la foreign key vers le type de filament
        //Pour l'instant, une impression peut avoir qu'un filament
        //Dans le futur, faudra peut-etre changer ca pour du multi-filament.

        Schema::table('impressions', function(Blueprint $table) {
            $table->unsignedInteger('filament_id')
                ->references('id')
                ->on('filaments')
                ->onDelete('restrict')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('impressions', function(Blueprint $table) {
            $table->dropColumn('filament_id');
        });
    }
}
