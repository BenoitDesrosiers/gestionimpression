<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;
use App\Models\Permission;

class UsersTableSeeder extends Seeder {

    public function run()
    {

        $roleadmin = Role::where('name', '=', 'admin')->first();
        $roleutilisateur  = Role::where('name', '=', 'utilisateur')->first();


        DB::table('users')->delete();
        $user = new User();
        $user->name = 'Admin';
        $user->username = 'Admin';

        $user->nom = 'systeme';
        $user->prenom = 'admin';
        $user->email = 'admin@chose.com';
        $user->password = Hash::make('usager');
        $user->save();
        $user->attachRole($roleadmin);

        $user = new User();
        $user->name = 'user1';
        $user->username = 'user1';

        $user->nom = 'user1 ';
        $user->prenom = 'un';
        $user->email = 'user1@chose.com';
        $user->password = Hash::make('usager');
        $user->save();
        $user->attachRole($roleutilisateur);

        $user = new User();
        $user->name = 'user2';
        $user->username = 'user2';

        $user->nom = 'user2 ';
        $user->prenom = 'un';
        $user->email = 'user2@chose.com';
        $user->password = Hash::make('usager');
        $user->save();
        $user->attachRole($roleutilisateur);

    }
}
