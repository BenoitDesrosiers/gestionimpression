<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Impression;
use Illuminate\Support\Carbon;


class ImpressionTableSeeder extends Seeder {

    public function run()
    {

        DB::table('impressions')->delete();

        //********** user 1
        $user = User::where('name', '=', 'user1')->first();

        $impression = new Impression();
        $impression->nom = 'impression 1';
        $impression->description = 'description 1';
        $impression->url = 'un url';
        $impression->couleur = "noir";
        $impression->resultat = 'bien imprimé, pas de problèmes';
        $impression->config = "couche 0.2, temp buse: 200c";
        $impression->tempsDebut = Carbon::now()->toIso8601ZuluString();
        $impression->tempsFin = Carbon::now()->addHours(1)->toIso8601ZuluString();

        $user->impressions()->save($impression);

        $impression = new Impression();
        $impression->nom = 'impression 2';
        $impression->description = 'description 2';
        $impression->url = 'deux url';
        $impression->couleur = "noir";
        $impression->resultat = 'bien imprimé, pas de problèmes';
        $impression->config = "couche 0.2, temp buse: 200c";
        $impression->tempsDebut = Carbon::now()->toIso8601ZuluString();
        $impression->tempsFin = Carbon::now()->addHours(1)->toIso8601ZuluString();
        $user->impressions()->save($impression);

        //************* user 2
        $user = User::where('name', '=', 'user2')->first();

        $impression = new Impression();
        $impression->nom = 'impression 3';
        $impression->description = 'description 3';
        $impression->url = 'deux url';
        $impression->couleur = "noir";
        $impression->resultat = 'bien imprimé, pas de problèmes';
        $impression->config = "couche 0.2, temp buse: 200c";
        $impression->tempsDebut = Carbon::now()->toIso8601ZuluString();
        $impression->tempsFin = Carbon::now()->addHours(1)->toIso8601ZuluString();
        $user->impressions()->save($impression);

        $impression = new Impression();
        $impression->nom = 'impression 4';
        $impression->description = 'description 4';
        $impression->url = 'deux url';
        $impression->couleur = "noir";
        $impression->resultat = 'bien imprimé, pas de problèmes';
        $impression->config = "couche 0.2, temp buse: 200c";
        $impression->tempsDebut = Carbon::now()->toIso8601ZuluString();
        $impression->tempsFin = Carbon::now()->addHours(1)->toIso8601ZuluString();
        $user->impressions()->save($impression);

    }
}
