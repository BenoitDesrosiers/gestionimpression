<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ImpressionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'nom' => 'required',
            'description'=> 'required',
            'tempsDebut'=> 'required',
            'tempsFin' => 'required'
        ];
    }

    public function withValidator($validator)
    {
        $validator->sometimes('filamentid', 'exists:Filaments,id', function ($input) {
            return $input->filamentid > 0;
        });
    }

}
