<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Impression;
use App\Models\User;
use App\Models\Filament;

use Auth;
use App\Http\Resources\ImpressionResource;
use App\Http\Resources\ReservationResource;
use App\Http\Requests\ImpressionRequest;
use Illuminate\Support\Carbon;


class ImpressionController extends Controller
{
    /**
     * Liste les impressions pour un usager ou pour tout les usagers
     *
     * @param Request $request
     * @param $id : l'id de l'usager pour qui lister les impressions
     *              0 = toutes les impressions
     *              -1 = l'usager connecté
     *              autre valeur = l'id d'un usager.
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection : la liste des impressions.
     */
    public function listePourUsagerApi(Request $request, $id) {

        //TODO: ajouter try catch
        if($id == -1) {
            $u = Auth::user();
            $i = Impression::where('proprietaire_id', $u->id)->get();
        } elseif($id == 0) {
            $i = Impression::all();
        } else {
            $u = User::find($id);
            $i = Impression::where('proprietaire_id', $u->id)->get();
        }
        ImpressionResource::withoutWrapping();
        return ImpressionResource::collection($i);
    }



    public function getApi(Request $request, $id) {
        ImpressionResource::withoutWrapping();
        return new ImpressionResource(Impression::find($id));
    }

    /**
     * @param ImpressionRequest $request
     * @param $id
     */
    public function storeApi(ImpressionRequest $request, $id) {
        $user = Auth::user();
        $impression = Impression::find($id);
        if(($impression->proprietaire->id <> $user->id) && !$user->hasRole('admin')) {
            abort(403);
        }
        $this->modifieImpression($impression, $request)->save();
        $impression->save();
    }

    public function createApi(ImpressionRequest $request) {
        $impression = new Impression();
        $proprietaire = Auth::user();
        $proprietaire->impressions()->save($this->modifieImpression($impression, $request));
        $proprietaire->impressions()->save($impression);
        return response($impression->id, 200);

    }

    public function modifieImpression( $impression, $request) {
        $impression->nom= $request['nom'];
        $impression->description= $request['description'];
        $impression->url= $request['url'];
        $impression->couleur= $request['couleur'];
        $impression->resultat= $request['resultat'];
        $impression->config= $request['configuration'];
        $impression->tempsDebut = (new Carbon($request['tempsDebut']))->toIso8601ZuluString(); //j'utilise carbon pour valider la date
        $impression->tempsFin = (new Carbon($request['tempsFin']))->toIso8601ZuluString();
        if($request['filamentid'] > 0) {
            $impression->filament_id = $request['filamentid']; //filamentid est validé dans le request
        } elseif ($request['filamentid'] == 0 ) {
            $impression->filament_id = null;
        }
        return $impression;
    }

    public function deleteApi(Request $request, $id) {
        $user = Auth::user();

        $impression = Impression::find($id);
        if (($impression->proprietaire->id <> $user->id) && !$user->hasRole('admin')) {
            abort(403);
        }

        $impression->delete();
        return response()->json();
    }

    public function dupliquerApi(Request $request, $id) {
        $proprietaire = Auth::user();
        $original = Impression::find($id);
        $dup = $original->replicate();
        $dup->tempsDebut = ""; //met les temps de réservation à vide pour pas créer de duplication de réservation.
        $dup->tempsFin = "";
        $proprietaire->impressions()->save($dup);

    }

    public function reservations(Request $request, $date, $periode) {
        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        if($periode=="week") {
            $debutPeriode = (new Carbon($date))->startOfWeek();
            $finPeriode = (new Carbon($date))->startOfWeek()->addDays(8);
        } else {
            $debutPeriode = (new Carbon($date))->startOfMonth()->startOfWeek();
            $finPeriode = (new Carbon($date))->startOfMonth()->startOfWeek()->addDays(36); //vue-simple-calendar affiche 35 jours, je veux donc le jour d'après à minuit
        }
        $i = Impression::whereBetween('tempsDebut',[$debutPeriode->toIso8601ZuluString(), $finPeriode->toIso8601ZuluString()])->get();
        ReservationResource::withoutWrapping();
        $r = ReservationResource::collection($i);
        return $r;
    }
}
