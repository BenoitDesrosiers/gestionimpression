<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Filament;
use App\Http\Requests\FilamentRequest;
use App\Models\Impression;

use App\Http\Resources\FilamentPourSelecteurResource;
use App\Http\Resources\FilamentResource;

class FilamentController extends Controller
{
    /**
     * Liste les filaments
     *
     */
    public function listeApi() {
        FilamentResource::withoutWrapping();
        return FilamentResource::collection(Filament::all());
    }

    /**
     * Liste tous les filaments pour un selecteur permettant de choisir un filament afin de l'associer.
     *
     * @param Request $request
     * @param
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection : la liste des filaments.
     */

    public function listePourSelecteurApi(Request $request) {
        FilamentPourSelecteurResource::withoutWrapping();
        $f = Filament::all();
        return FilamentPourSelecteurResource::collection($f);
    }

    public function getApi(Request $request, $id) {
        FilamentResource::withoutWrapping();
        return new FilamentResource(Filament::find($id));
    }

    /**
     * @param FilamentRequest $request
     * @param $id
     */
    public function storeApi(FilamentRequest $request, $id) {
        $item = Filament::find($id);
        $this->modifieItem($item, $request)->save();
    }

    public function createApi(FilamentRequest $request) {
        $item = new Filament();
        $this->modifieItem($item, $request)->save();
        return response($item->id, 200);
    }

    public function modifieItem( $filament, $request) {
        $filament->fabricant= $request['fabricant'];
        $filament->diametre= $request['diametre'];
        $filament->temperature_recommandee= $request['temperature_recommandee'];
        $filament->commentaire= $request['commentaire'];
        return $filament;
    }

    public function deleteApi(Request $request, $id) {
        $item = Filament::find($id);
        $item->delete();
        return response()->json();
    }
    /**
     *
     * Crée un filament, et l'associe à l'impression qui est passé dans le request.
     *
     * @param FilamentRequest $request
     *      impression_id: l'id de l'impression à associer
     *      fabricant, diametre, temperature_recommandee, commentaire: les attributs du filament.
     */
    public function creerEtAssocierApi(FilamentRequest $request) {
        $impression = Impression::find($request['impression_id']);
        $filament = new Filament();
        $filament->fabricant = $request['fabricant'];
        $filament->diametre = $request['diametre'];
        $filament->temperature_recommandee = $request['temperature_recommandee'];
        $filament->commentaire = $request['commentaire'];
        $filament->save();
        $filament->impressions()->save($impression);
    }
}
