<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;

use App\Http\Resources\FilamentPourSelecteurResource;
use App\Http\Resources\UserResource;

class UserController extends Controller
{
    /**
     * Liste les usagers
     *
     */
    public function listeApi() {
        UserResource::withoutWrapping();
        return UserResource::collection(User::all());
    }

    /**
     * Retourne l'usager ayant cet $id
     * @param Request $request
     * @param $id
     * @return UserResource
     */
    public function getApi(Request $request, $id) {
        UserResource::withoutWrapping();
        return new UserResource(User::find($id));
    }

    /**
     * Efface l'usager ayant cet $id
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */

    public function deleteApi(Request $request, $id) {
        $usagerConnecte = Auth::user();
        //Seul un admin peut effacer des usagers.
        if(!$usagerConnecte->hasRole('admin')) {
            abort(403);
        }
        $item = User::find($id);
        $item->delete();
        return response()->json();
    }

}
