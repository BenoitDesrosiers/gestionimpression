<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Auth;
use Illuminate\Support\Carbon;


class ReservationResource extends JsonResource
{
    /**
     * Retourne une impression dans le format d'un event pour vue-simple-calendar
     * https://github.com/richardtallent/vue-simple-calendar
     *
     *
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => (string)$this->id,
            'title'             => $this->proprietaire->prenom . ' ' .  $this->proprietaire->nom . ' - ' .$this->nom,
            //ajoute une seconde pour pas avoir minuit, car c'est mal affiché dans vue-simple-calendar
            //transforme en EST parce que j'ai pas trouvé comment le faire dans vue-simple-calendar, au diable la version internationale :)
            'startDate'        =>  (new Carbon($this->tempsDebut))->addSeconds(1)->setTimezone('EST')->toIso8601String(),
            'endDate'          => (new Carbon($this->tempsFin))->addSeconds(1)->setTimezone('EST')->toIso8601String(),
        ];
    }
}
