<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Auth;
use Datetime;
use DatetimeZone;

class ImpressionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $u = Auth::user();
        $d = $this->created_at;
        $d->setTimezone('EST');  //Force le format en eastern standard time. (la date est stockée en GW)
        $tempsDebut = DateTime::createFromFormat('Y-m-d\TH:i:sO',$this->tempsDebut);
        $tempsDebut->setTimezone(new DateTimeZone('EST'));  //Force le format en eastern standard time. (la date est stockée en GW)
        $tempsFin = DateTime::createFromFormat('Y-m-d\TH:i:sO',$this->tempsFin);
        $tempsFin->setTimezone(new DateTimeZone('EST'));  //Force le format en eastern standard time. (la date est stockée en GW)

        return [

            'id'                => (string)$this->id,
            'proprietaireId'    => (string)$this->proprietaire_id,
            'estProprietaire'   => $this->proprietaire_id == $u->id || $u->hasRole('admin'),
            'proprietaireNom'   => $this->proprietaire->prenom . ' ' .  $this->proprietaire->nom,
            'nom'               => $this->nom,
            'description'       => $this->description,
            'url'               => $this->url,
            'couleur'           => $this->couleur,
            'resultat'          => $this->resultat,
            'configuration'     => $this->config,
            'tempsDebut'        => $tempsDebut->format('Y-m-d H:i'),
            'tempsFin'          => $tempsFin->format('Y-m-d H:i'),
            'dateCreation'      => date('Y-m-d H:i', strtotime($d)),
            'filamentid'        => is_null($this->filament_id)?0:$this->filament_id,

        ];
    }
}
