<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => (string)$this->id,
            'username'      => $this->username,
            'prenom'        => $this->prenom,
            'nom'           => $this->nom,
            'courriel'      => $this->email,
        ];
    }
}
