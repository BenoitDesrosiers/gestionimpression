<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FilamentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => (string)$this->id,
            'fabricant'     => $this->fabricant,
            'diametre'      => (string)$this->diametre,
            'temperature_recommandee' => (string)$this->temperature_recommandee,
            'commentaire'   => $this->commentaire
        ];
    }
}
