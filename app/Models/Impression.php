<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Filament;

class Impression extends Model
{
    // mass assignment protection
    protected $guarded = array('id');

    // Database relationship
    public function proprietaire() {
        return $this->belongsTo( User::class );
    }

    public function filament() {
        return $this->belongsTo( Filament::class);
    }
}
