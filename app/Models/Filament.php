<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Impression;

class Filament extends Model
{
    // mass assignment protection
    protected $guarded = array('id');

    // Database relationship
    public function impressions() {
        return $this->hasMany( Impression::class );
    }
}
