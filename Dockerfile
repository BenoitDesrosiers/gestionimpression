FROM node as build
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY webpack.mix.js .
COPY resources ./resources
RUN npm run prod

FROM php:7 as runtime
RUN apt-get update -y && apt-get install -y openssl zip unzip git
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN docker-php-ext-install pdo mbstring pdo_mysql
WORKDIR /app
COPY --from=build /app/public /app/public
COPY . .
RUN composer install
RUN cp -f .env.prod .env
RUN php artisan key:generate --force
RUN chmod +x run-prod.sh
CMD ["./run-prod.sh"]